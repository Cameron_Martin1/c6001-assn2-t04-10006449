#**GUI Individual Journal Entry**

> The team leader assigned me with the tasks of;

* Authorization
* Terms and Conditions
* Applying to be a Facebook Developer

##**Authorization**
> According to Facebook, when collecting information directly from Facebook users the party collecting the data must make it clear that you (and not Facebook) are collecting it. A notice requesting to obtain user consent must also be provided, if the person denies this request then you must not proceed further. In New Zealand the Privacy Act 1993 also states that the parties undergoing data collection must have given consent. 

> In our application we will be requesting user authorization to access their Facebook information.
> We will tell them exactly what we are doing with their information, what we will use it for and if it will be stored. I feel that this is necessary because we are taking someone’s information and using it, without authorisation it would be theft and I don’t ethically feel that it is right. Our application will be created in accordance to New Zealand and Facebook privacy laws.

http://www.legislation.govt.nz/act/public/1993/0028/latest/whole.html#DLM297094

https://www.facebook.com/page_guidelines.php

https://www.facebook.com/about/privacy

##**Terms and Conditions**
> When using Facebook’s API, the organisation or party involved must adhere to Facebook’s set Terms and Conditions.
> In the Facebook Developers website and when creating a Developer account, by signing up you’re agreeing to abide by the Terms. 
These terms are summarised into 22 sections, some of which that are relevant to our application include;

+ Build a quality application
    - This means building an app that is stable and easily navigable.
    - Does not confuse, deceive, defraud, mislead, spam or surprise.
+ Give people control.
    - Obtain consent from people before publishing content on their behalf.
    - Provide meaningful customer support to your application.
    - Allow users to opt out, in which their info gets deleted.
+ Protect data
    - Protect the information you receive from Facebook against unauthorized access, use, or disclosure.
    - Don't sell, license, or purchase any data obtained.
+ Encourage proper use
    - Respect the way Facebook looks and functions. Don't offer experiences that change it.
    - Don’t build an app whose primary purpose is to redirect people off of Facebook.

> I have read through the Terms and Conditions and completely agree with Facebook. Facebook are offering Developers the chance to optimise their application by allowing access to their services. The guidelines set are very essential and allow the developer to be creative with their application to an extent.

https://developers.facebook.com/policy/

#Developer Application

> Applying to become a Facebook Developer is easy and anyone can do it. Facebook’s Developer services are not only restricted to users logging in but also services such as:
> ![services](https://s26.postimg.org/upkx56ri1/Screen_Shot_2017-09-10_at_9.20.22_PM.png) 
> After navigating to the Facebook developers website, you need to login using your Facebook account. When creating account you will be required to fill in some basic information as well as accept to their Terms and Conditions. After that process is complete you will be taken to the homepage where you are able to create, modify and link applications using Facebook’s API as well as get support from Facebook and the Developers Group.

https://developers.facebook.com/tools-and-support

