# **_Milestone 1: Planning Report_**

## **_Jaskarn Singh, Sam Halligan, Cameron Martin_**

### **Our Project**

For our project we plan to make a simple web application. This application will pull information from Facebook. This means we will be using the Facebook API. As a group we decided this API was something we were interested in and wanted to learn more about. Especially since we are all advid users of Facebook. Further this gives us a chance to learn more about the code and processes in Web design and application of Facebook.

### **Tools used and Why?**

#### **Facebook Messenger**

We will be using Facebook Messenger as our main form of communication. This is mainly due to the ease of use and accessibility. We all use messenger and always check it. Logically this is a better method then slack. However because we are required to use slack. I will make sure we copy paste all information and conversation about the group project to the slack channel. This will include all key details like meetings and role delegations.

#### **Slack**

Although I said Facebook Messenger would be our main form of communication. During class or times when we are at the ploytechnic we will make sure to use slack and share any relevant information/resources we find there. Slack has a lot of unique functions like being able to pin posts/links. We will be using this function to keep track of important information and resorces for our project.

#### **Trello**

Facebook and Slack offer a very good method of communication. However we needed something more planning based to keep track of progress. Trello is a planning and management web application. This allowed our team to physical list all tasks to get a better look at progress. As a tool this is very necessary for us. Esepcially for the leader to delegate rolls by using the drag feature trello provides. Trello allows us to efficiently plan ahead and see where we are with progress. As I mentioned before with the drag function we can move our tasks from in progress to completed. Making it very easy to see what features of the application need to be worked on. All three tools are necessary to our api application.

[Trello - Team 4](https://trello.com/b/SN5UaZIn)

### **Wireframe**

We have come up with a simple wireframe showcasing our application. We as a group acknowledge we are not very good at programming. So we decided to stick with a simplistic program that will cover the requirements of the assessment. Looking briefly over the funcions and coding required. And more at what we want out of the app. This relates to what we hope to achieve through this application.

[Wireframe for our App](https://drive.google.com/a/bcs.net.nz/file/d/0B8YB6--euYpOTDkwUG9sbXVEZTA/view?usp=sharing)

#### **Key Information about the application**

1. Functions
2. Terms and Conditions
3. Facebook API
4. ASPCORE.NET and API
5. Authorization
6. User Interface Considerations
7. Aesthetics 
8. Components
9. Updating data or refreshing

All the above will be researched and added to our collected information. Allowing any one of us to be able to complete any task. The main idea is we want this web application to appear professional. This is why we have gone with a similar colourization and theme as Facebook itself. This serves us two purposes. One it allows the user to see clearly what this application requires. While further attracting viewers with the layout.

#### **Planning and Key Components**

All the planning stages below will be placed into our trello board. This will allow delegation and further managment of our application.

1. Researching the above 9 points. This will be delegated to the three of us. (This stage will occur before milestone 2 but is important relating to planning)
    1. Functions
    2. Terms and Conditions
    3. Facebook API
    4. ASPCORE.NET and API
    5. Authorization
    6. User Interface Considerations
    7. Aesthetics 
    8. Components
    9. Updating data or refreshing
    10. Any Images or Aethetical information required
    11. Apply for Facebook Developer. Allowing us to use the API.
2. Starting off we will need to make each of the main three components. This relates to the two main pages and the pop-up page. We will split the coding for this between us.
    1. ASPCORE.NET MVC created. All correct settings.
    2. Create the Login Page.
    3. Create the Home Page.
    4. Create the Login Pop-up Page.
    5. Code is labelled and easy to follow. Notes included.
3. Next another one of us will make sure the images/layout of the pages match the wireframe.
    1. Compare Layouts to Web Application
    2. Check we collected all images and information relating to layout and aesthetics.
    3. Aesthetical Considerations.
    4. Images and Colour scheme will match facebook as per above.
    5. Confirm between all members.
5. Interactive buttons/links and making sure the web application flows and is functional.
    1. Login button is working as needed.
    2. Web application can login and logout.
    3. Web application signup link works for new users.
    4. Smooth transition between pages.
    5. Code is labelled and easy to follow. Notes included.
6. Code for the web app to be compatible on multiple platforms.
    1. Responsive web pages. Works on all platforms.
    2. tests on multiple platforms.
    3. Resolved any distortions.
    4. Code is labelled and easy to follow. Notes included.
7. Integrate Facebook API into the web app.
    1. API Coding setup. This relates to the Important code related to the Facebook API.
    2. Code/ methods to extract information from the user profile.
    3. Testing code
    4. Code is labelled and easy to follow. Notes included.
8. Incorporate key GUI elements into designing the Web app.
    1. These elements will be from our research.
    2. Apply key changes to make our web application more effective.
    3. Aesthetic considerations for GUI elements and design.
9. Data can be refreshed through the refresh button.
    1. Data refresh button is created.
    2. All code related to data being refreshed on the page.
    3. Code is labelled and easy to follow. Notes Included.

### **Roles of Group Members**

In order to manage the Assignments workload effectively, each of the three team members have been tasked with different parts of the project:

* Jaskarn being the group leader is managing the group by hosting the repository as well as User interface, Facebook API and Aspcore.net and API consderations Research.
* Sam will be a support team member tasked with doing Functions, Aesthetics, Components and Data Refresh key research.
* Cameron will also a support team member tasked with Authorization, Terms and Conditions as well as Applying to be a Facebook Developer research.

Although the roles above at this stage relate to research. Through trello Jaskarn will delegate tasks further once we progress to further stages. Through leadership, careful planning and time managment we will make sure to create a decent application using the facebook API. All the delegated research findings will be delegated to each members individual journal report. These will be forked to the main team report. This allows any problems to be resolved quickly as all information is easily accessible and any member can do any part.